//
//  TableViewModel.h
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableViewModel : NSObject<UITableViewDataSource>

- (NSDictionary *) getDataAtIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
