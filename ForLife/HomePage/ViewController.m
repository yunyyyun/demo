//
//  ViewController.m
//  ForLife
//
//  Created by meng yun on 2021/4/13.
//

#import "ViewController.h"
#import "TableViewModel.h"
@interface ViewController ()<UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) TableViewModel *vm;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-1)];
    [self.view addSubview: _tableView];
    _tableView.delegate = self;
    // _tableView.dataSource = self;
    
    _vm = [[TableViewModel alloc] init];
    _tableView.dataSource = _vm;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: true];
    NSLog(@"viewWillDisappear---:");
}

- (void)receiveNoti:(NSNotification *)notification{
    NSDictionary *userInfo=notification.userInfo;
    NSLog(@"receiveNoti---: %@ %@", userInfo[@"key"], [NSThread currentThread]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = [_vm getDataAtIndexPath: indexPath];
    NSString *func = [dic valueForKey: @"func"];
    if ([func isEqual: @""]) {
        return 33;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *dic = [_vm getDataAtIndexPath: indexPath];
    NSString *func = [dic valueForKey: @"func"];
    Class AnyController = NSClassFromString(func);
    NSObject *vc = [[AnyController alloc]init];
    if ([vc isKindOfClass: [UIViewController class]]) {
        [self.navigationController pushViewController: (UIViewController *)vc animated: true];
    }
}


@end
