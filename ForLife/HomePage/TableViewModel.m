//
//  TableViewModel.m
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import "TableViewModel.h"

@interface TableViewModel ()

@property (nonatomic, strong) NSMutableArray<NSArray<NSDictionary *> *> *dataSource;

@end

@implementation TableViewModel

- (instancetype) init{
    self = [super init];
    if (self) {
        [self initDatas];
    }
    return self;
}

- (void) initDatas {
    NSArray * d0 = @[
        @{@"title": @"------------------> Metal",  @"func": @""},
        @{@"title": @"1-图片渲染",  @"func": @"MRIViewController"},
        @{@"title": @"2-并行计算高斯模糊",  @"func": @"GBViewController"},
        @{@"title": @"3-并行计算迭代处理",  @"func": @"CycleViewController"},
        @{@"title": @"4-Julia集",  @"func": @"JuliaViewController"},];
    
    NSArray * d1 = @[
        @{@"title": @"------------------> OpenGL",  @"func": @""},
        @{@"title": @"1-fl",  @"func": @"OPGLViewController"},
        @{@"title": @"2-OpenGL-Compute Shader",  @"func": @"OPGLViewController"},];
    
    NSArray * d2 = @[
        @{@"title": @"------------------> OC",  @"func": @""},
        @{@"title": @"1-justTest",  @"func": @"TestViewController"},
        @{@"title": @"2-load",  @"func": @"LoadViewController",},
        @{@"title": @"3-weakTimer",  @"func": @"WeakTimerViewController"},
        @{@"title": @"4-多线程",  @"func": @"MTViewController"},
        @{@"title": @"5-锁",  @"func": @"LockViewController"},
        @{@"title": @"6-异步绘图",  @"func": @"AsyncDrawViewController"},
        @{@"title": @"7-runloop",  @"func": @"RunloopViewController"},
    ];
        
    self.dataSource = [@[d0, d1, d2]
                       mutableCopy];
}

- (NSDictionary *) getDataAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _dataSource[indexPath.section][indexPath.row];
    return dic;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource[section].count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    NSDictionary *dic = _dataSource[indexPath.section][indexPath.row];
    NSString *title = [dic valueForKey: @"title"];
    cell.textLabel.text = title;
    NSString *func = [dic valueForKey: @"func"];
    if ([func isEqual: @""]) {
        cell.backgroundColor = [UIColor systemGreenColor];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}

@end
