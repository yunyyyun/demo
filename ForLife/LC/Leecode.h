//
//  Leecode.h
//  ForLife
//
//  Created by dc on 2021/6/14.
//

#ifndef Leecode_h
#define Leecode_h

#include <stdio.h>
#include <stdlib.h>
#include "stdbool.h"

struct ListNode {
    char* val;
    struct ListNode *next;
};
struct ListNode *creatListNode(const char* data);
struct ListNode* reverseList(struct ListNode* head);
void printListNode(struct ListNode *head);

bool isPalindrome(struct ListNode* head);


typedef
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
} TreeNode;

TreeNode *creatBTNode(int data);
TreeNode *creatBTTree(void);
void print0(TreeNode *root); // 父左右

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q);
#endif /* Leecode_h */
