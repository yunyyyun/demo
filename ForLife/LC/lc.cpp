//
//  lc.cpp
//  ForLife
//
//  Created by dc on 2022/4/22.
//

#include "lc.hpp"

class Node {
public:
    int val;
    Node* left;
    Node* right;

    Node() {}

    Node(int _val) {
        val = _val;
        left = NULL;
        right = NULL;
    }

    Node(int _val, Node* _left, Node* _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};

class Solution {
    Node help[100];
public:
    Node* treeToDoublyList(Node* root) {
        Node nodes[100];
        
        int n = 0;
        int end = 0;
        Node *head = NULL;
        while (root && end >= 0) {
            if (root->left) {
                nodes[end++] = *root;
                root = root->left;
            } else if (root->right) {
                printf("-%d-", root->val);
                help[n++] = *root;
                root = root->right;
            } else {
                printf("-%d-", root->val);
                help[n++] = *root;
                end = end-1;
                if (head == NULL) {
                    head = root;
                }
                if (end >= 0) {
                    Node node = nodes[end];
                    root->right = &node;
                    printf("-%d-", node.val);
                    help[n++] = node;
                    root = (node.right);
                }
                
            }
        }
        for(int i=0;i<n-1;i++){ //向后依次连接
            help[i].right = &help[i+1];
        }
        for(int i=n-1;i>0;i--){ //向前依次连接
            help[i].left = &help[i-1];
        }
        help[n-1].right= &help[0];
        help[0].left = &help[n-1];
        return &help[0];
    }
    
    void midFirst(Node *root) {
        
    }
};
