//
//  Leecode.c
//  ForLife
//
//  Created by dc on 2021/6/14.
//

#include "Leecode.h"

struct ListNode *creatListNode(const char* data) {
    struct ListNode *node = (struct ListNode *)malloc(sizeof(struct ListNode));
    node->val = data;
    node->next = NULL;
    return node;
}

void printListNode(struct ListNode *head) {
    struct ListNode *cur = head;
    while (cur != NULL) {
        printf("->%s ", cur->val);
        cur = cur->next;
    }
    printf("\n");
}

// 1-2-3-4
struct ListNode* reverseList(struct ListNode* head){

    struct ListNode *pre = NULL;
    struct ListNode *cur = head;
    while (cur != NULL) {
        struct ListNode *next = cur->next;
        cur->next = pre;

        pre = cur;
        cur = next;
    }
    return pre;
}

bool isPalindrome(struct ListNode* head){
    struct ListNode* p0 = head;
    struct ListNode* p1 = head;
    while (p0!=NULL && p1!=NULL) {
        p0 = p0->next;
        p1 = p1->next->next;
    }
    return false;
}

TreeNode *creatBTNode(int data) {
    TreeNode *node = (TreeNode *)malloc(sizeof(TreeNode));
    node->val = data;
    node->left = NULL;
    node->right = NULL;
    return node;
}

TreeNode *creatBTTree(void) {
    TreeNode *node5 = creatBTNode(5);
    TreeNode *node4 = creatBTNode(4);
    TreeNode *node8 = creatBTNode(8);
    TreeNode *node11 = creatBTNode(11);
    TreeNode *node13 = creatBTNode(13);
    TreeNode *node44 = creatBTNode(4);
    TreeNode *node7 = creatBTNode(7);
    TreeNode *node2 = creatBTNode(2);
    TreeNode *node55 = creatBTNode(5);
    TreeNode *node1 = creatBTNode(1);
    node5->left = node4;
    node5->right = node8;
    
    node4->left = node11;
    node8->left = node13;
    node8->right = node44;
    
    node11->left = node7;
    node11->right = node2;
    node44->left = node55;
    node44->right = node1;
    return node5;
}

void print(TreeNode *root) {
    printf("->%d", root->val);
}

void print0(TreeNode *root) {
    if (root == NULL) {
        return;
    }
    print(root);
    print0(root->left);
    print0(root->right);
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if (root == NULL || p == NULL || q == NULL) {
        return NULL;
    }
    if (root->val == p->val || root->val == q->val) {
        return root;
    }
    TreeNode *left = lowestCommonAncestor(root->left, p, q);
    TreeNode *right = lowestCommonAncestor(root->right, p, q);
    if(left != NULL && right != NULL){
        return root;
    }
    return left != NULL ? left : right;
}
