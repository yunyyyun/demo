//
//  SceneDelegate.h
//  ForLife
//
//  Created by meng yun on 2021/3/12.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

