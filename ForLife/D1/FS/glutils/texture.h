#ifndef __GLUTILS_TEXTURE_H__
#define __GLUTILS_TEXTURE_H__

#include "gldefine.h"

namespace glutils {
    class Texture final {
    public:
        Texture(GLenum target = GL_TEXTURE_2D);
        ~Texture();

        void createFloatTextureWith();
        
        void createFloat16(int width, int height, int filterType = GL_LINEAR, int wrapType = GL_CLAMP_TO_EDGE);

    public:
        int _width;
        int _height;
        int _format;
        GLenum _target;
        GLuint _textureID;
    };
} // namespace glutils

#endif
