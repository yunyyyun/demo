#include "./Simulation.h"

namespace glutils {

    Simulation::Simulation() {
    }

    Simulation::~Simulation() {
        safeDelete(tmpTexture);
    }

    void Simulation::initOpenGL(const TextureInfo &inTex) {
        int width = 600.0;
        int height = 600.0;

        if (tmpTexture == nullptr) {
            tmpTexture = new Texture();
            tmpTexture->createFloat16(width, height);
        }

    }

    void Simulation::preProcessing(const TextureInfo &inTex, const TextureInfo &outTex) {

        drawToTexture(outTex, [&] {
            static const char *vsh =
            #include "../glutils/passthrough_vsh.glsl"
            static const char *fsh =
            #include "../fluid/prep_fsh.glsl"
            Program *pass = getSharedProgram(0, vsh, fsh);
            pass->use();
            pass->setUniformTexture("uTexture0", 0, inTex.textureID);
            pass->setUniform1i("deltaT", delta);
            return pass;
        });    
    }

    void Simulation::stepFrameTest(int dt, const TextureInfo &outTex) {

        // printf("-- %d \n", dt);
        
        drawToTexture(outTex, [&] {
            static const char *vsh =
            #include "../glutils/passthrough_vsh.glsl"
            static const char *fsh =
            #include "../fluid/update_fsh.glsl"
            Program *pass = getSharedProgram(1, vsh, fsh);
            pass->use();
            pass->setUniformTexture("uTexture0", 0, outTex.textureID);
            pass->setUniform1i("deltaT", delta);
            return pass;
        });
                
    }


} // namespace glutils
