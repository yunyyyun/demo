R"(

precision mediump float;
varying vec2 vTexCoord;
uniform sampler2D uTexture0;
uniform int deltaT;

int isLive(vec4 color) {
    float r = color.x;
    float g = color.y;
    float b = color.z;
    float y = 0.299*r+0.587*g+0.114*b;
    if (y>0.1) {
        return 1;
    } else {
        return 0;
    }
}

void main()
{
    vec4 color = texture2D(uTexture0, vTexCoord);
    gl_FragColor = color;
    
    if (deltaT == 1) {
        return;
    }
    
    float d = 1.0/float(deltaT);
    vec2 ct = vTexCoord;
    if (ct.x <= d || ct.y <= d || ct.x >= 1.0-d|| ct.y >= 1.0-d) {
        gl_FragColor = vec4(0.09, 0.09, 0.09, 1.0);
        return;
    }
    float x = vTexCoord.x;
    float y = vTexCoord.y;
    x = x - mod(x, d);
    y = y - mod(y, d);
    vec2 newPosion = vec2(x + d/2.0, y + d/2.0);
    vec4 newColor = texture2D(uTexture0, newPosion);
    
    vec2 up    = vec2(ct.x,   ct.y-d);
    vec2 left  = vec2(ct.x-d, ct.y);
    vec2 down  = vec2(ct.x,   ct.y+d);
    vec2 right = vec2(ct.x+d, ct.y);

    int liveCount = // getY(texture2D(uTexture0, ct)) +
    isLive(texture2D(uTexture0, vec2(ct.x,   ct.y-d))) + // up
    isLive(texture2D(uTexture0, vec2(ct.x-d, ct.y))) +   //left
    isLive(texture2D(uTexture0, vec2(ct.x,   ct.y+d))) + // down
    isLive(texture2D(uTexture0, vec2(ct.x+d, ct.y))) +   // right
    isLive(texture2D(uTexture0, vec2(ct.x-d, ct.y-d))) +
    isLive(texture2D(uTexture0, vec2(ct.x+d, ct.y-d))) +
    isLive(texture2D(uTexture0, vec2(ct.x+d, ct.y+d))) +
    isLive(texture2D(uTexture0, vec2(ct.x-d, ct.y+d)));
        
    vec4 dieColor = vec4(0.09, 0.09, 0.09, 1.0);
    vec4 liveColor = vec4(40.0/255.0, 128.0/255.0, 20.0/255.0, 1.0);
    if (liveCount == 3) { // “繁殖”：任何死细胞如果活邻居正好是3个，则活过来
        gl_FragColor = liveColor;
    } else if (isLive(newColor)==1 && liveCount < 2) { // “人口过少”：任何活细胞如果活邻居少于2个，则死掉。
        gl_FragColor = dieColor;
    } else if (isLive(newColor)==1 && (liveCount == 3 || liveCount == 2)) { // “正常”：任何活细胞如果活邻居为2个或3个，则继续活
       gl_FragColor = liveColor;
    } else if (isLive(newColor)==1 && liveCount>3) { // “人口过多”：任何活细胞如果活邻居大于3个，则死掉。
        gl_FragColor = dieColor;
     }
}

)";
