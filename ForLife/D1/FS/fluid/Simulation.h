#ifndef __GLUTILS_PROCESSOR_H__
#define __GLUTILS_PROCESSOR_H__

#include "GLPlayground.h"

namespace glutils {

    class Simulation : public GLPlayground {
    public:
        Simulation();
        ~Simulation();

        void initOpenGL(const TextureInfo &inTex);
        
        void preProcessing(const TextureInfo &inTex, const TextureInfo &outTex);
        void stepFrameTest(int dt, const TextureInfo &outTex);

        Simulation &operator=(const Simulation &rhs) = delete;
        Simulation(const Simulation &rhs) = delete;

    private:
        Texture *tmpTexture = nullptr;
        int delta = 64;
    };
} // namespace glutils

#endif
