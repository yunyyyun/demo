R"(

precision mediump float;
varying vec2 vTexCoord;
uniform sampler2D uTexture0;
uniform int deltaT;

float getY(vec4 color) {
    float r = color.x;
    float g = color.y;
    float b = color.z;
    float y = 0.299*r+0.587*g+0.114*b;
    return y;
}

void main()
{
    vec4 color = texture2D(uTexture0, vTexCoord);
    if (deltaT<0) {
        return;
    }
 
    float d = 1.0/float(deltaT);
    float x = vTexCoord.x;
    float y = vTexCoord.y;
    x = x - mod(x, d);
    y = y - mod(y, d);
    vec2 newPosion = vec2(x + d/2.0, y + d/2.0);
    vec4 newColor = texture2D(uTexture0, newPosion);
    
    vec4 dieColor = vec4(0.09, 0.09, 0.09, 1.0);
    vec4 liveColor = vec4(40.0/255.0, 128.0/255.0, 20.0/255.0, 1.0);
    if (sin(getY(newColor)*6.283) > 0.5){
        gl_FragColor = liveColor;
    } else {
        gl_FragColor = dieColor;
    }
}

)";
