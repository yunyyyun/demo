//
//  OPGLViewController.m
//  ForLife
//
//  Created by meng yun on 2021/4/15.
//

#import "OPGLViewController.h"
#import "GLTexture.h"
#include "Simulation.h"
#import <vector>

using namespace::glutils;

static glutils::TextureInfo toFlowImageTexture(GLTexture *texture) {
    glutils::TextureInfo inTex;
    inTex.textureID = texture.textureID;
    inTex.width = texture.width;
    inTex.height = texture.height;
    inTex.target = texture.target;
    return inTex;
}

static CGRect computeViewport(CGRect rect, GLTexture *texture) {
    CGRect viewport = rect;
    CGFloat sx = rect.size.width / (CGFloat)texture.width;
    CGFloat sy = rect.size.height / (CGFloat)texture.height;
    CGFloat s = MIN(sx, sy);

    CGFloat width = (CGFloat)texture.width * s;
    CGFloat height = (CGFloat)texture.height * s;

    viewport.origin.x = (rect.size.width - width) * 0.5;
    viewport.origin.y = (rect.size.height - height) * 0.5;
    viewport.size.width = width;
    viewport.size.height = height;
    return viewport;
}

@interface OPGLViewController ()

@property (strong, nonatomic) UIImageView *previewView;

@end

@implementation OPGLViewController {
    EAGLContext *_context;
    GLTexture *_inputTexture;
    GLTexture *_outputTexture;
    
    CGFloat _screenWidth;
    CGFloat _screenHeight;
    Simulation *_simulation;
    
    int _stepTime;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _simulation = new Simulation();
    self.view.backgroundColor = [UIColor greenColor];
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    if (!_context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = _context;
    view.enableSetNeedsDisplay = FALSE;
    
    _previewView = [[UIImageView alloc] init];
    [self.view addSubview: _previewView];
    _previewView.frame = CGRectMake(0, 100, 100, 100);
    
    _screenWidth = [UIScreen mainScreen].bounds.size.width;
    _screenHeight = [UIScreen mainScreen].bounds.size.height;
    view.contentScaleFactor = [UIScreen mainScreen].scale;
    
    [self setupOpenGL];
    _simulation->initOpenGL(toFlowImageTexture(_inputTexture));
}

- (void)setupOpenGL {
    [EAGLContext setCurrentContext:_context];
    glClearColor(0.2, 0.2, 0.2, 1.0);

    UIImage *image = [UIImage imageNamed:@"t1024"];
    _inputTexture = [[GLTexture alloc] initWithTarget:GL_TEXTURE_2D];
    _outputTexture = [[GLTexture alloc] initWithTarget:GL_TEXTURE_2D];
    [_inputTexture createWithImage: image];
    [_outputTexture createWithWidth:_inputTexture.width height:_inputTexture.height format:GL_RGBA];
    
    _previewView.image = image;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClear(GL_COLOR_BUFFER_BIT);

    CGRect viewport = computeViewport(rect, _outputTexture);

    CGFloat scale = self.view.contentScaleFactor;
    viewport.origin.x *= scale;
    viewport.origin.y *= scale;
    viewport.size.width *= scale;
    viewport.size.height *= scale;

    glViewport(viewport.origin.x, viewport.origin.y, viewport.size.width, viewport.size.height);

    glutils::TextureInfo inTex = toFlowImageTexture(_outputTexture);
    _simulation->drawTexture(inTex, true);
}

- (void)dealloc {
    if (_simulation) {
        delete _simulation;
        _simulation = nullptr;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: true];
    _stepTime = 0;
    _simulation->preProcessing(toFlowImageTexture(_inputTexture), toFlowImageTexture(_outputTexture));
    [self timerManager: _stepTime];
}

- (void)timerManager:(double)animated {
    glutils::TextureInfo outTxt = toFlowImageTexture(_outputTexture);
    _stepTime+=1;
    if (_stepTime > 3) {
        _stepTime = 1;
    }
    _simulation->stepFrameTest(_stepTime, outTxt);
    GLKView *view = (GLKView *)self.view;
    [view display];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self timerManager: self->_stepTime];
    });
}


@end
