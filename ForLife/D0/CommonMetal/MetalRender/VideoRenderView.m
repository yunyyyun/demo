//
//  VideoRenderView.m
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#import "VideoRenderView.h"
#import "MetalVideoRender.h"

@interface VideoRenderView ()

@property (nonatomic, strong) MetalVideoRender * render;

@end

@implementation VideoRenderView

- (instancetype)initWithFrame:(CGRect)frame shaderFuncName: (NSString *) sfn
{
    self = [super initWithFrame:frame];
    if (self) {

        _render = [[MetalVideoRender alloc] initWith: self
                                      shaderFuncName: sfn];
    }
    return self;
}

- (void)displayImage:(UIImage *)image{
    [_render displayImage: image];
}

@end
