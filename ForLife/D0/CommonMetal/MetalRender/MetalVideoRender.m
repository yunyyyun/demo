//
//  MetalVideoRender.m
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/13.
//

@import Metal;
@import MetalKit;
@import simd;
@import ModelIO;

#import "MetalVideoRender.h"
#import "ShaderTypes.h"

@interface MetalVideoRender ()<MTKViewDelegate>

@property (nonatomic, strong) NSString *shaderName;
@property (nonatomic, assign) float delta;
@property (nonatomic, weak) VideoRenderView * mtView;

@property (nonatomic, assign) CVMetalTextureCacheRef textureCache;
@property (nonatomic, assign) vector_uint2 viewportSize;

@property (nonatomic, strong) id<MTLDevice> device;
@property (nonatomic, strong) id<MTLCommandQueue> commandQueue;
@property (nonatomic, strong) id<MTLRenderPipelineState> pipelineState;
@property (nonatomic, strong) id<MTLDepthStencilState> depth;

@property (nonatomic, strong) id<MTLBuffer> uniformsBuffer;
@property (nonatomic, strong) id<MTLTexture> diffuseTexture;
@property (nonatomic, strong) id<MTLSamplerState> samplerTexture;

@property (nonatomic, strong) id<MTLBuffer> vertices;
@property (nonatomic, assign) NSUInteger numVertices;

@end

@implementation MetalVideoRender

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupMetal];
    }
    return self;
}

- (instancetype)initWith:(VideoRenderView *)view shaderFuncName: (NSString *) sfn {
    self = [super init];
    if (self) {
        self.shaderName = sfn;
        [self setMetalView: view];
        CVMetalTextureCacheCreate(NULL, NULL, self.mtView.device, NULL, &_textureCache); // TextureCache的创建
        [self setupMetal];
        self.delta = 0;
        self.viewportSize = (vector_uint2){self.mtView.drawableSize.width, self.mtView.drawableSize.height};

    }
    return self;
}

- (void)setMetalView:(VideoRenderView *)view {
    self.mtView = view;
    
    [view setOpaque: false];
    view.framebufferOnly = false;
    view.device = MTLCreateSystemDefaultDevice();
    view.delegate = self;
}

-(void)setupMetal {
    [self setupPipeline];
    [self setupVertex];
}

// 设置渲染管线
-(void)setupPipeline {
    _device = _mtView.device;
    id<MTLLibrary> defaultLibrary = [_device newDefaultLibrary]; // .metal
    id<MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vertexShader"]; // 顶点 shader，vertexShader 是函数名
    id<MTLFunction> fragmentFunction = [defaultLibrary newFunctionWithName: self.shaderName]; // 片元 shader，fsh_main 是函数名
    
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = _mtView.colorPixelFormat;
    self.pipelineState = [_device newRenderPipelineStateWithDescriptor: pipelineStateDescriptor
                                                                 error: NULL]; // 创建图形渲染管道，耗性能操作不宜频繁调用
    self.commandQueue = [_device newCommandQueue]; // CommandQueue是渲染指令队列，保证渲染指令有序地提交到GPU
}

// 设置顶点缓存
- (void)setupVertex {
    static const LYVertex quadVertices[] =
    {   // 顶点坐标，分别是x、y、z、w；    纹理坐标，x、y；
        { {  1.0, -1.0, 0.0, 1.0 },  { 1.f, 1.f } },
        { { -1.0, -1.0, 0.0, 1.0 },  { 0.f, 1.f } },
        { { -1.0,  1.0, 0.0, 1.0 },  { 0.f, 0.f } },
        
        { {  1.0, -1.0, 0.0, 1.0 },  { 1.f, 1.f } },
        { { -1.0,  1.0, 0.0, 1.0 },  { 0.f, 0.f } },
        { {  1.0,  1.0, 0.0, 1.0 },  { 1.f, 0.f } },
    };
    self.vertices = [_device newBufferWithBytes: quadVertices
                                                     length:sizeof(quadVertices)
                                                    options:MTLResourceStorageModeShared]; // 创建顶点缓存
    self.numVertices = sizeof(quadVertices) / sizeof(LYVertex); // 顶点个数
}

- (void)displayImage:(UIImage *)image {
    // NSLog(@"drawInMTKView--00 %lf", self.delta);

    //
    id <CAMetalDrawable> drawable = _mtView.currentDrawable;
    MTLRenderPassDescriptor *renderPassDescriptor = _mtView.currentRenderPassDescriptor;
    if (renderPassDescriptor && drawable) {
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0, 0.5, 0.5, 1.0f);
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture;
        renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        
        id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
        id<MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
        [renderEncoder setViewport:(MTLViewport){0.0, 0.0, self.viewportSize.x, self.viewportSize.y, -1.0, 1.0 }]; // 设置显示区域
        [renderEncoder setRenderPipelineState: _pipelineState];
        [renderEncoder setVertexBuffer:self.vertices
                                offset:0
                               atIndex:0]; // 设置顶点缓存
        
        Complex cd;
        cd.image = -1 ;
        cd.real = 0;
        id<MTLBuffer> cdBufer = [_device newBufferWithBytes: &cd
                                             length: sizeof(Complex)
                                                        options:MTLResourceStorageModeShared];
        [renderEncoder setVertexBuffer: cdBufer
                                offset: 0
                               atIndex: 1]; // 设置顶点缓存

        // [self setupTextureWithEncoder:renderEncoder buffer: pixelBuffer];
        MTKTextureLoader *loader = [[MTKTextureLoader alloc] initWithDevice: _device];
        id<MTLTexture> textureLookUp = [loader newTextureWithCGImage: image.CGImage
                                             options: @{MTKTextureLoaderOptionSRGB:@(NO)}
                                               error: nil];
        
        [renderEncoder setFragmentTexture: textureLookUp
                            atIndex: 0];
        


        [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangle
                          vertexStart:0
                          vertexCount:self.numVertices]; // 绘制

        [renderEncoder endEncoding]; // 结束

        [commandBuffer presentDrawable: drawable];
        [commandBuffer commit];
    }
}

- (void)drawInMTKView:(nonnull MTKView *)view {
    // NSLog(@"drawInMTKView--11 nouse");
//    _delta += 0.0002;
//    [self displayImage: [UIImage imageNamed: @"t0"]];
    
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
    self.viewportSize = (vector_uint2){size.width, size.height};
}


@end
