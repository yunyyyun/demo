//
//  VideoRenderView.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoRenderView : MTKView

- (instancetype)initWithFrame:(CGRect)frame shaderFuncName: (NSString *) sfn;
- (void)displayImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
