//
//  MetalVideoRender.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/13.
//

#import <UIKit/UIKit.h>
// #import <CoreVideo/CVPixelBuffer.h>
#import "VideoRenderView.h"

@interface MetalVideoRender : NSObject

- (instancetype)initWith:(VideoRenderView *)view shaderFuncName: (NSString *) sfn;
- (void)displayImage:(UIImage *)image;
// - (instancetype)initWith: (VideoRenderView *)view;

@end

