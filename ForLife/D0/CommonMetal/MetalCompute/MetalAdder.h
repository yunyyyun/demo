/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A class to manage all of the Metal objects this app creates.
*/

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import "ShaderTypes.h"

NS_ASSUME_NONNULL_BEGIN

@interface MetalAdder : NSObject

- (instancetype) initWithDevice: (id<MTLDevice>) device
                       funcName: (NSString *) fname;

- (void) prepareDataWith: (unsigned char *)data
                      CM: (float *)cm
                   width: (size_t)width
                  height: (size_t)height;

- (void) sendComputeCommand;

- (Clr *) getResult;

- (unsigned char *) getResult2;

@end

NS_ASSUME_NONNULL_END
