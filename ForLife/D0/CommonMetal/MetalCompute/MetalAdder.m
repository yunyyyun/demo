/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A class to manage all of the Metal objects this app creates.
*/

#import "MetalAdder.h"

@implementation MetalAdder
{
    // The number of floats in each array, and the size of the arrays in bytes.
    size_t arrayLength;
    size_t bufferSize;
    
    id<MTLDevice> _mDevice;
    
    // The compute pipeline generated from the compute kernel in the .metal shader file.
    id<MTLComputePipelineState> _mAddFunctionPSO;
    
    // The command queue used to pass commands to the device.
    id<MTLCommandQueue> _mCommandQueue;
    
    // Buffers to hold data.
    id<MTLBuffer> _mBufferA;
    id<MTLBuffer> _mBufferCM;
    id<MTLBuffer> _mBufferResult;

}

- (instancetype) initWithDevice: (id<MTLDevice>) device funcName: (NSString *) fname
{
    self = [super init];
    if (self)
    {
        _mDevice = device;
        
        NSError* error = nil;
        
        // Load the shader files with a .metal file extension in the project

        id<MTLLibrary> defaultLibrary = [_mDevice newDefaultLibrary];
        if (defaultLibrary == nil)
        {
            NSLog(@"Failed to find the default library.");
            return nil;
        }

        id<MTLFunction> addFunction = [defaultLibrary newFunctionWithName: fname];
        if (addFunction == nil)
        {
            NSLog(@"Failed to find the adder function.");
            return nil;
        }
        
        // Create a compute pipeline state object.
        _mAddFunctionPSO = [_mDevice newComputePipelineStateWithFunction: addFunction error:&error];
        if (_mAddFunctionPSO == nil)
        {
            //  If the Metal API validation is enabled, you can find out more information about what
            //  went wrong.  (Metal API validation is enabled by default when a debug build is run
            //  from Xcode)
            NSLog(@"Failed to created pipeline state object, error %@.", error);
            return nil;
        }
        
        _mCommandQueue = [_mDevice newCommandQueue];
        if (_mCommandQueue == nil)
        {
            NSLog(@"Failed to find the command queue.");
            return nil;
        }
    }
    
    return self;
}

- (void) prepareDataWith: (unsigned char *)data CM: (float *)cm width: (size_t)width height:(size_t)height
{
    arrayLength = width * height * 4;
    bufferSize = arrayLength * sizeof(unsigned char);
    
    _mBufferResult = [_mDevice newBufferWithLength: bufferSize
                                           options: MTLResourceStorageModeShared];
    
    _mBufferA = [_mDevice newBufferWithBytes: data
                                      length: bufferSize
                                     options: MTLResourceStorageModeShared];
    
    _mBufferCM = [_mDevice newBufferWithBytes: cm
                                      length: bufferSize
                                     options: MTLResourceStorageModeShared];
    
}

- (void) sendComputeCommand
{
    // Create a command buffer to hold commands.
    id<MTLCommandBuffer> commandBuffer = [_mCommandQueue commandBuffer];
    assert(commandBuffer != nil);
    
    // Start a compute pass.
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    assert(computeEncoder != nil);
    
    [self encodeAddCommand:computeEncoder];
    
    // End the compute pass.
    [computeEncoder endEncoding];
    
    // Execute the command.
    [commandBuffer commit];
    
    // Normally, you want to do other work in your app while the GPU is running,
    // but in this example, the code simply blocks until the calculation is complete.
    [commandBuffer waitUntilCompleted];

}

- (void)encodeAddCommand:(id<MTLComputeCommandEncoder>)computeEncoder {
    
    // Encode the pipeline state object and its parameters.
    [computeEncoder setComputePipelineState: _mAddFunctionPSO];
    [computeEncoder setBuffer: _mBufferA offset:0 atIndex:0];
    [computeEncoder setBuffer: _mBufferCM offset:0 atIndex:1];
    [computeEncoder setBuffer: _mBufferResult offset:0 atIndex:2];
    
    MTLSize gridSize = MTLSizeMake(arrayLength/4, 1, 1);
    
    // Calculate a threadgroup size.
    NSUInteger threadGroupSize = _mAddFunctionPSO.maxTotalThreadsPerThreadgroup;
    if (threadGroupSize > arrayLength)
    {
        threadGroupSize = arrayLength;
    }
    MTLSize threadgroupSize = MTLSizeMake(threadGroupSize, 1, 1);
    
    // Encode the compute command.
    [computeEncoder dispatchThreads:gridSize
              threadsPerThreadgroup:threadgroupSize];
}

- (Clr *) getResult {
    Clr* result = _mBufferResult.contents;
    // [self verifyResults];
    return result;
}

- (unsigned char *) getResult2 {
    unsigned char * result = _mBufferResult.contents;
    
    _mBufferA = nil;
    _mBufferCM = nil;
    _mBufferResult = nil;
    // [self verifyResults];
    return result;
}

//- (void) verifyResults
//{
//    Clr* a = _mBufferA.contents;
//    Clr* result = _mBufferResult.contents;
//
//    for (unsigned long index = 0; index < arrayLength; index++)
//    {
//        if (index < 99999 && index > 99888) {
//            Clr r = a[index];
//            Clr r2 = result[index];
//            printf("verifyResults %d: == (%d %d %d),(%d %d %d)\n", index, r.r, r.g, r.b,  r2.r, r2.g, r2.b);
//        }
//    }
//}

@end
