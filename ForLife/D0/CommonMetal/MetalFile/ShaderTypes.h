//
//  Header.h
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#ifndef ShaderTypes_h
#define ShaderTypes_h

#include <simd/simd.h>


typedef struct
{
    vector_float4 clipSpacePosition;
    vector_float2 textureCoordinate;
} LYVertex;

typedef struct
{
    float real;  // 实部
    float image; // 虚部
} Complex;

typedef struct
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} Clr;


#endif /* ShaderTypes_h */
