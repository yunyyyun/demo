//
//  TypeDefines.metal
//  ForLife
//
//  Created by meng yun on 2021/4/13.
//

#include <metal_stdlib>
using namespace metal;

constant float PI = 3.1416;

typedef struct
{
    float4 clipSpacePosition [[position]]; // position的修饰符表示这个是顶点
    float2 textureCoordinate; // 纹理坐标，会做插值处理
    float2 cd;
    
} UniformData; // 返回给片元着色器的结构体
