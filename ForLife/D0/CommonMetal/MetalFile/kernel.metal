//
//  Shaders.metal
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#include <metal_stdlib>
#import "ShaderTypes.h"

using namespace metal;

// rgb 求灰度值
unsigned char getGray(Clr c){
    return 0.299*c.r + 0.587*c.g + 0.114*c.b;
}

// rgb 求灰度值,并二阶化
unsigned char getSecoender(Clr c){
    unsigned char gray = 0.299*c.r + 0.587*c.g + 0.114*c.b;
    if (gray>128) {
        gray = 1;
    } else {
        gray = 0;
    }
    return gray;
}

// 并行计算数组处理
kernel void gaussian_arrays(device const Clr* inA,
                       device const float* inCM,
                       device Clr* result,
                       uint index [[thread_position_in_grid]])
{
    int w = inCM[0];
    int h = inCM[1];
    int br = inCM[2]; // 模糊半径
    int rw = inCM[3]; // 周围边长
    
    int ii = index/w; // 行
    int jj = index%w; // 列
    
    float r = 0;
    float g = 0;
    float b = 0;
    float a = 0;
    for (int i=0; i < rw; i++){
        for (int j=0; j < rw; j++){
            float weight = inCM[i+j*rw + 4];
            Clr clr;// = inA[w*(ii + i - br) + (jj + j - br)];
            int _i = ii + i - br;
            int _j = jj + j - br;
            if (_i <= 1 || _i >= h-1 ||
                _j <= 1 || _j >= w-1) { // 边缘处理
                unsigned char av = (inA[index].r + inA[index].g + inA[index].b)/16;
                clr = {av,av,av,64};
            } else {
                clr = inA[w*_i + _j];
            }
            r = r+ clr.r * weight;
            g = g+ clr.g * weight;
            b = b+ clr.b * weight;
            a = a+ clr.a * weight;
        }
    }
    result[index].r = r;
    result[index].g = g;
    result[index].b = b;
    result[index].a = a;
}

// 黑白二阶
kernel void seconder_arrays(device const Clr* inA,
                       device const float* inCM,
                       device Clr* result,
                       uint index [[thread_position_in_grid]])
{
    Clr c = inA[index];
    unsigned char gray = c.r/3 + c.g/3 + c.b/3;
    int d = inCM[3];
    unsigned char rlt = (1 + (int)gray/d)*d - 1;
    result[index].r = rlt*1;
    result[index].g = rlt*1;
    result[index].b = rlt*1;
    result[index].a = 255;
    return;
}

kernel void test_arrays(device const Clr* inA,
                       device const float* inCM,
                       device Clr* result,
                       uint index [[thread_position_in_grid]])
{
    int w = inCM[0];
    // int h = inCM[1];
    
    int i = index/w; // 行
    int j = index%w; // 列
    int d = 4;
    
    int _i = (i/d)*d;
    int _j = (j/d)*d;
    
    int newindex = _i*w + _j;
    Clr clr = inA[newindex];
    
    float r = clr.r;
    float g = clr.g;
    float b = clr.b;
    float a = clr.a;
    result[index].r = r;
    result[index].g = g;
    result[index].b = b;
    result[index].a = a;
    return;
}
