//
//  julia.metal
//  ForLife
//
//  Created by meng yun on 2021/4/13.
//

#include <metal_stdlib>
#import "ShaderTypes.h"
#import "defines.metal"
using namespace metal;

constant float3 colortab[21] = {
    {28, 28, 28},
    {130, 130, 130},
    {85, 26, 139},
    {224, 102, 255},
    {255, 187, 255},
    {0, 0, 205},
    {72, 118, 255},
    {0, 191, 255},
    {0, 255, 255},
    {0, 255, 127},
    {0, 255, 0},
    {50, 205, 50},
    {173, 255, 47},
    {255, 185, 15},
    {255, 215, 0},
    {255, 255, 0},
    {255, 69, 0},
    {255, 140, 0},
    {255, 211, 155},
    {255, 231, 186},
    {0, 0, 0},
};

float3 dye(float dist){
    float s = 1.0/4096.0;
    for (int i=0; i<21; ++i){
        if (dist < s) {
            return colortab[i];
        }
        s = s*2;
    }
    return {0, 0, 0};
}

float _model(Complex a)
{
    return sqrt(a.real * a.real + a.image * a.image);
}

Complex _multiply(Complex a, Complex b)
{
    Complex c;
    c.real = a.real * b.real - a.image * b.image;
    c.image = a.image * b.real + a.real * b.image;
    return c;
}

float _iteration(Complex a, float c, float d, int n)
{
    if(n==0)
        return _model(a);
    else
    {
        Complex temp = _multiply(a, a);
        temp.real += c;
        temp.image += d;
        return _iteration(temp,c,d, n-1);
    }
}

// julia 集
fragment float4
fsh_julia(UniformData input [[stage_in]],
               texture2d<float> texture [[ texture(0)]])
{
    float2 st = input.textureCoordinate;
    
    float x = (st.x - 0.5)*4;
    float y = (st.y - 0.5)*4;
    Complex a;
    a.real = x;
    a.image = y;
    
    float c = input.cd.x;
    float d = input.cd.y;
    float dist = _iteration(a,c,d, 13);
    float3 color = dye(dist)/256.0;
    return float4(color, 1);
}
