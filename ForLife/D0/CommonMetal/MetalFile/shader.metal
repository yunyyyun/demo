//
//  Shaders.metal
//  AmzFFPlayer
//
//  Created by meng yun on 2021/1/14.
//

#include <metal_stdlib>
#import "ShaderTypes.h"
#import "defines.metal"
using namespace metal;

// 顶点着色器
vertex UniformData
vertexShader(uint vertexID [[ vertex_id ]], // vertex_id是顶点shader每次处理的index，用于定位当前的顶点
             constant LYVertex *vertexArray [[ buffer(0) ]],
             constant Complex&   u   [[buffer(1)]]) { // buffer表明是缓存数据，0是索引
    UniformData out;
    out.clipSpacePosition = vertexArray[vertexID].clipSpacePosition;
    out.textureCoordinate = vertexArray[vertexID].textureCoordinate;
    out.cd = float2(u.image, u.real);
    return out;
}

// 图片渲染
fragment float4
fsh_normal(UniformData input [[stage_in]],
               texture2d<float> texture [[ texture(0)]])
{
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear);
    float2 st = input.textureCoordinate;
    float4 textureColor = texture.sample(textureSampler, st);
    return textureColor;
}
