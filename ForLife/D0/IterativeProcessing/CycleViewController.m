//
//  CycleViewController.m
//  ForLife
//
//  Created by meng yun on 2021/4/2.
//

#import "CycleViewController.h"
#import "VideoRenderView.h"
#import <Metal/Metal.h>
#import "MetalAdder.h"

@interface CycleViewController ()

@property (strong, nonatomic) VideoRenderView *mtRenderView;
@property (strong, nonatomic) MetalAdder *adder;
@property (assign, nonatomic) float *cmArr;
@property (assign, nonatomic) int d;

@end

@implementation CycleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor systemGrayColor];
    
    CGRect f = self.view.frame;
    self.mtRenderView = [[VideoRenderView alloc] initWithFrame: CGRectMake(0,
                                                                           80,
                                                                           f.size.width,
                                                                           f.size.height - 80)
                                                shaderFuncName: @"fsh_normal"];
    [self.view addSubview: self.mtRenderView];
    
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    _adder = [[MetalAdder alloc] initWithDevice:device funcName: @"seconder_arrays"];
    _d = 16;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    UIImage *img0 = [UIImage imageNamed: @"cyc"];
    [self test: img0];
}

- (void)test:(UIImage *)image {
    UIImage *newImage = [self image2Image: image];
    [self.mtRenderView displayImage: newImage];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self test: newImage];
    });
}

- (UIImage *)image2Image:(UIImage *)image {
    CGImageRef cgImageRef = image.CGImage;
    size_t width = CGImageGetWidth(cgImageRef); // 图片宽度
    size_t height = CGImageGetHeight(cgImageRef); // 图片高度
    unsigned char *data = calloc(width * height * 4, sizeof(unsigned char)); // 取图片首地址
    size_t bitsPerComponent = 8; // r g b a 每个component bits数目
    size_t bytesPerRow = width * 4; // 一张图片每行字节数目 (每个像素点包含r g b a 四个字节)
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB(); // 创建rgb颜色空间
    CGContextRef context = CGBitmapContextCreate(data,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 space,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgImageRef);

    [self convertCharsByMetal: data width: width height: height];
    
    cgImageRef = CGBitmapContextCreateImage(context);
    UIImage *result = [UIImage imageWithCGImage: cgImageRef];
    
    CFRelease(cgImageRef);
    free(data);
    data = NULL;
    
    return result;
}

-(void) convertCharsByMetal: (unsigned char *)data width:(size_t)width height:(size_t)height {
    int br = 16;
    _d += 1;
    if (_d>255) {
        _d = 1;
    }
    NSLog(@"_d_is: %d", _d);
    if (_cmArr == nil) {
        _cmArr = calloc(width * height * 4, sizeof(float)); // 高斯模糊卷积矩阵
        _cmArr[0] = width;
        _cmArr[1] = height;
        _cmArr[2] = br;
        _cmArr[3] = _d;
    }
    
    [self getWeightMatrixWithR: br inArr: _cmArr];

    float vf = 0.0;
    for (int i = 4; i< (2*br+1)*(2*br+1)+4+1 ; ++i){
        vf+= _cmArr[i];
    }
    NSLog(@"metalLog: 图片大小(%lf,%lf) 高斯模糊半径(%lf,%lf)  %lf", _cmArr[0],_cmArr[1], _cmArr[2],_cmArr[3], vf);

    [_adder prepareDataWith: data CM: _cmArr width: width height:height];
    [_adder sendComputeCommand];

    unsigned char *r = [_adder getResult2];
    memcpy(data, r, height * width * 4);

    r = NULL;
    free(r);
    
}

// br：模糊半径
- (void) getWeightMatrixWithR: (int)br inArr:(float *)weightArr {
    int size = br*2+1;
    float sum = 0;
    for (int i=0;i < size;i++){
        for (int j=0;j < size;j++){
            int index = i*size + j;
            float weight = [self getWeightWithR:br x: j-br y: br-i];
            weightArr[index + 4] = weight;
            sum += weight;
        }
    }
    for (int i=0;i < size;i++){
        for (int j=0;j < size;j++){
            int index = i*size + j;
            weightArr[index + 4]/=sum;
            // weightArr[index + 4] = 1.0/size/size;
        }
    }
}

- (float)getWeightWithR: (int)br x:(int) x y:(int) y {
    float pi = M_PI;
    float e = M_E;
    float sigma = 1.5;
    float t = ((-(x*x+y*y)) / ((2*sigma*sigma)));
    float weight = (1/(2*pi*sigma*sigma))*pow(e,t);
    return weight;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
