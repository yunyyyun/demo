//
//  GBViewController.m
//  ForLife
//
//  Created by meng yun on 2021/3/12.
//

#import "GBViewController.h"
#import "VideoRenderView.h"
#import <Metal/Metal.h>
#import "MetalAdder.h"
#import "CycleViewController.h"

@interface GBViewController ()

@property (strong, nonatomic) VideoRenderView *mtRenderView;
@property (strong, nonatomic) UILabel *mtLabel;

@property (strong, nonatomic) VideoRenderView *cpuRenderView;
@property (strong, nonatomic) UILabel *cpuLabel;

@end

@implementation GBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor systemGrayColor];
    CGRect f = self.view.frame;
    self.mtRenderView = [[VideoRenderView alloc] initWithFrame: CGRectMake(0,
                                                                           80,
                                                                           f.size.width,
                                                                           f.size.width) shaderFuncName: @"fsh_normal"];
    [self.view addSubview: self.mtRenderView];
    self.mtLabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, 200, 44)];
    self.mtLabel.textColor = [UIColor systemRedColor];
    self.mtLabel.text = @"metal";
    self.mtLabel.font = [UIFont systemFontOfSize: 28];
    [self.mtRenderView addSubview: self.mtLabel];
    
    self.cpuRenderView = [[VideoRenderView alloc] initWithFrame: CGRectMake(0,
                                                                            f.size.height - f.size.width,
                                                                            f.size.width,
                                                                            f.size.width)
                                                 shaderFuncName: @"fsh_normal"];
    [self.view addSubview: self.cpuRenderView];
    self.cpuLabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, 200, 44)];
    self.cpuLabel.textColor = [UIColor systemRedColor];
    self.cpuLabel.text = @"cpu";
    self.cpuLabel.font = [UIFont systemFontOfSize: 28];
    [self.cpuRenderView addSubview: self.cpuLabel];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    dispatch_queue_t hehe = dispatch_queue_create("hehe", DISPATCH_QUEUE_CONCURRENT);
    UIImage *img0 = [UIImage imageNamed: @"t10242"];
    NSLog(@"图片大小：%zu, %zu", CGImageGetWidth(img0.CGImage), CGImageGetHeight(img0.CGImage));
    
    dispatch_async(hehe, ^{
        CFAbsoluteTime curTime = CFAbsoluteTimeGetCurrent();
        UIImage * imgMetal = [self image2Image: img0 useMetal: true];
        UIImage * imgMetal2 = [self image2Image: imgMetal useMetal: true];
        CFAbsoluteTime costTime = (CFAbsoluteTimeGetCurrent() - curTime);

        [self.mtRenderView displayImage: imgMetal2];
        NSLog(@"metal 耗时：%f ms",costTime * 1000.0);
        // self.mtLabel.text = [NSString stringWithFormat: @"metal 耗时：%f ms", costTime * 1000.0];
        // UIImageWriteToSavedPhotosAlbum(imgMetal, self, NULL, NULL);
    });
    
    dispatch_async(hehe, ^{
        CFAbsoluteTime curTime = CFAbsoluteTimeGetCurrent();
        // UIImage * imgCpu = [self image2Image: img0 useMetal: false];
        UIImage * imgCpu = [self image2Image_test: img0];
        CFAbsoluteTime costTime = (CFAbsoluteTimeGetCurrent() - curTime);
        [self.cpuRenderView displayImage: imgCpu];
        NSLog(@"cpu 耗时：%f ms",costTime * 1000.0);
        // self.cpuLabel.text = [NSString stringWithFormat: @"cpu 耗时：%f ms", costTime * 1000.0];
        // UIImageWriteToSavedPhotosAlbum(imgCpu, self, NULL, NULL);
    });
    
}

- (UIImage *)image2Image_test:(UIImage *)image {
    CIFilter *filter = [CIFilter filterWithName: @"CIGaussianBlur"];
    CIImage *inputCIImage = [[CIImage alloc] initWithImage: image];
    
    [filter setValue: inputCIImage forKey:kCIInputImageKey];
    [filter setValue:@(16) forKey:@"inputRadius"];
    //
    CIImage *outPutImage = [filter outputImage];
    //获取上下文
    CIContext *context = [CIContext contextWithOptions: nil];
    
    CGImageRef cgImage = [context createCGImage:outPutImage fromRect:outPutImage.extent];
    
    UIImage *filter_image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    return filter_image;
}

- (UIImage *)image2Image:(UIImage *)image useMetal: (BOOL)useMetal {
    UIImage *newImage = [self drawWithWithImage: image];
    CGImageRef cgImageRef = newImage.CGImage;
    size_t width = CGImageGetWidth(cgImageRef); // 图片宽度
    size_t height = CGImageGetHeight(cgImageRef); // 图片高度
    unsigned char *data = calloc(width * height * 4, sizeof(unsigned char)); // 取图片首地址
    size_t bitsPerComponent = 8; // r g b a 每个component bits数目
    size_t bytesPerRow = width * 4; // 一张图片每行字节数目 (每个像素点包含r g b a 四个字节)
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB(); // 创建rgb颜色空间
    CGContextRef context = CGBitmapContextCreate(data,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 space,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgImageRef);

    if (useMetal) {
        [self convertCharsByMetal: data width: width height: height];
    } else {
        [self convertChars: data width: width height: height];
    }
    
    cgImageRef = CGBitmapContextCreateImage(context);
    UIImage *result = [UIImage imageWithCGImage: cgImageRef];
    CFRelease(cgImageRef);
    return result;
}

- (UIImage *)drawWithWithImage:(UIImage *)originImage {
    CGImageRef cgImageRef = originImage.CGImage;
    size_t width = CGImageGetWidth(cgImageRef); // 图片宽度
    size_t height = CGImageGetHeight(cgImageRef); // 图片高度
    CGFloat maxWidth = 512;
    CGSize size = CGSizeMake(maxWidth, maxWidth);
    if (width > maxWidth){
        size = CGSizeMake(maxWidth, maxWidth*height/width);
    } else {
        return originImage;
    }
    //这里设置为0，意为自动设置清晰度，图片可以是别的传过来的图片信息
    UIGraphicsBeginImageContextWithOptions(size, NO,0);
    [originImage drawInRect: CGRectMake(0, 0, size.width, size.height)];
    originImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return originImage;
}

-(void) convertChars: (unsigned char *)data width:(size_t)width height:(size_t)height {
    for (size_t i = 0; i < height; i++)
    {
        for (size_t j = 0; j < width; j++)
        {
            size_t pixelIndex = i * width * 4 + j * 4;
            size_t d = 8;
            size_t ii = (i/d) * d;
            size_t jj = (j/d) * d;
            size_t newIndex = width * ii * 4 + jj * 4;
            unsigned char red = data[newIndex];
            unsigned char green = data[newIndex + 1];
            unsigned char blue = data[newIndex + 2];
            data[pixelIndex] = red;
            data[pixelIndex + 1] = green;
            data[pixelIndex + 2] = blue;
        }
    }
}

-(void) convertCharsByMetal: (unsigned char *)data width:(size_t)width height:(size_t)height {
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    
    float *cmArr = calloc(width * height * 4, sizeof(float)); // 高斯模糊卷积矩阵
    int br = 16;
    cmArr[0] = width;
    cmArr[1] = height;
    cmArr[2] = br;
    cmArr[3] = 2*br+1;
    [self getWeightMatrixWithR: br inArr: cmArr];
    
    float vf = 0.0;
    for (int i = 4; i< (2*br+1)*(2*br+1)+4+1 ; ++i){
        vf+= cmArr[i];
    }
    NSLog(@"metalLog: 图片大小(%lf,%lf) 高斯模糊半径(%lf,%lf)  %lf",cmArr[0],cmArr[1],cmArr[2],cmArr[3], vf);
    
    MetalAdder* adder = [[MetalAdder alloc] initWithDevice:device funcName: @"gaussian_arrays"];
    [adder prepareDataWith: data CM: cmArr width: width height:height];
    [adder sendComputeCommand];
    
    unsigned char *r = [adder getResult2];
    memcpy(data, r, height * width * 4);
}

// br：模糊半径
- (void) getWeightMatrixWithR: (int)br inArr:(float *)weightArr {
    int size = br*2+1;
    float sum = 0;
    for (int i=0;i < size;i++){
        for (int j=0;j < size;j++){
            int index = i*size + j;
            float weight = [self getWeightWithR:br x: j-br y: br-i];
            weightArr[index + 4] = weight;
            sum += weight;
        }
    }
    for (int i=0;i < size;i++){
        for (int j=0;j < size;j++){
            int index = i*size + j;
            weightArr[index + 4]/=sum;
            // weightArr[index + 4] = 1.0/size/size;
        }
    }
}

- (float)getWeightWithR: (int)br x:(int) x y:(int) y {
    float pi = M_PI;
    float e = M_E;
    float sigma = (br*2+1)/2;
    float t = ((-(x*x+y*y)) / ((2*sigma*sigma)));
    float weight = (1/(2*pi*sigma*sigma))*pow(e,t);
    return weight;
}

- (IBAction)clickToNext:(id)sender {
    
    CycleViewController *vc = [[CycleViewController alloc] init];
    [self.navigationController pushViewController: vc animated: true];
    
}

@end
