//
//  JuliaViewController.m
//  ForLife
//
//  Created by meng yun on 2021/4/13.
//

#import "JuliaViewController.h"
#import "VideoRenderView.h"
@interface JuliaViewController ()

@property (strong, nonatomic) VideoRenderView *mtRenderView;

@end

@implementation JuliaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor systemGrayColor];
    CGRect f = self.view.frame;
    self.mtRenderView = [[VideoRenderView alloc] initWithFrame: CGRectMake(0,
                                                                           80,
                                                                           f.size.width,
                                                                           f.size.height - 80)
                                                shaderFuncName: @"fsh_julia"];
    [self.view addSubview: self.mtRenderView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self.mtRenderView displayImage: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
