//
//  main.m
//  ForLife
//
//  Created by meng yun on 2021/3/12.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Leecode.h"
#include "lc.hpp"
// 3 1
int peakIndexInMountainArray(int* arr, int arrSize){
    int d = arrSize/2;
    int index = arrSize/2;
    while (index && d) {
        int mid = arr[index];
        printf("%d: %d %d\n", index, mid, d);
        if (mid >= arr[index-1] && mid >= arr[index+1]) {
            return index;
        }
        d = (d+1)/2;
        if (mid > arr[index-1]) {
            index = index + d;
        } else {
            index = index - d;
        }
    }
    return 0;
}

int main(int argc, char * argv[]) {

//    int arr[29] = {1,57,58,74,88,93,98,97,96,91,90,78,77,74,71,68,61,50,42,38,35,34,26,20,15,14,5,4,2};
//    printf("----: %d \n\n", peakIndexInMountainArray(arr, 29));
    
    
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
        NSLog(@"log_613 @autoreleasepool");
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}



