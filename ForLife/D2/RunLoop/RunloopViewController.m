//
//  RunloopViewController.m
//  ForLife
//
//  Created by dc on 2021/7/17.
//

#import "RunloopViewController.h"
CFRunLoopObserverRef rrr;
@interface RunloopViewController ()

@property(nonatomic, strong) NSThread *thread;

@property(nonatomic, assign) bool shouldKeepRunning;

@property(nonatomic, strong) NSTimer *timer;

@end

@implementation RunloopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    // [self addRunloopObserver];
    
    UIButton *btn0 = [[UIButton alloc] initWithFrame: CGRectMake(12, 122, 88, 44)];
    btn0.backgroundColor = [UIColor systemRedColor];
    [btn0 addTarget: self action: @selector(startThread:) forControlEvents: UIControlEventTouchUpInside];
    [self.view addSubview: btn0];
    
    UIButton *btn01 = [[UIButton alloc] initWithFrame: CGRectMake(12, 200, 88, 44)];
    btn01.backgroundColor = [UIColor systemBlueColor];
    [btn01 addTarget: self action: @selector(stopThread:) forControlEvents: UIControlEventTouchUpInside];
    [self.view addSubview: btn01];
    
    self.timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSLog(@"timer fire");
    }];
}

- (void)startThread:(UIButton*)sender {
    self.shouldKeepRunning = YES;
    self.thread = [[NSThread alloc] initWithTarget:self selector:@selector(runNow) object:nil];
    [self.thread start];
}

- (void)runNow {

    self.timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSLog(@"timer fire");
    }];
    [[NSRunLoop currentRunLoop] addTimer: self.timer forMode:(NSDefaultRunLoopMode)];

    NSRunLoop *theRL = [NSRunLoop currentRunLoop];

    while (self.shouldKeepRunning && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
}

- (void)stopThread:(UIButton*)sender {

    if (self.thread && !self.thread.isFinished) {

        [self performSelector:@selector(_realStopHeartBeat) onThread:self.thread withObject:nil waitUntilDone:YES];

        [self.thread cancel];
       // [self.thread s];
    }
}

- (void)_realStopHeartBeat {

//    if(self.timer) {
//
//        [self.timer invalidate];
//        self.timer = nil;
//    }
    CFRunLoopStop(CFRunLoopGetCurrent());

    self.shouldKeepRunning = NO;
}


- (void)addRunloopObserver {
    // 1.获取当前Runloop
    CFRunLoopRef runloop = CFRunLoopGetCurrent();
    
    // 2.创建观察者
    
    // 2.0 定义上下文
    CFRunLoopObserverContext context = {
        0,
        (__bridge void *)(self),
        &CFRetain,
        &CFRelease,
        NULL
    };
    
    // 2.1 定义观察者
    static CFRunLoopObserverRef defaultModeObserver;
    // 2.2 创建观察者
    defaultModeObserver = CFRunLoopObserverCreate(kCFAllocatorDefault,
                                                  kCFRunLoopAllActivities,
                                                  YES,
                                                  0,
                                                  &callBack,
                                                  &context);
    
    // 3. 给当前Runloop添加观察者
    // CFRunLoopMode mode : 设置任务执行的模式
    CFRunLoopAddObserver(runloop, defaultModeObserver, kCFRunLoopCommonModes);
    
    rrr = defaultModeObserver;
    // CFRunLoopAddObserver(<#CFRunLoopRef rl#>, <#CFRunLoopObserverRef observer#>, <#CFRunLoopMode mode#>)
    // CFRunLoopRemoveObserver(<#CFRunLoopRef rl#>, <#CFRunLoopObserverRef observer#>, <#CFRunLoopMode mode#>)
    // C中出现 copy,retain,Create等关键字,都需要release
    CFRelease(defaultModeObserver);
}

static void callBack(CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *info) {
   
    switch (activity) {
        case kCFRunLoopEntry:
            printf("callback---kCFRunLoopEntry \n");
            break;
        case kCFRunLoopBeforeTimers:
            printf("callback---kCFRunLoopBeforeTimers \n");
            break;
        case kCFRunLoopBeforeSources:
            printf("callback---kCFRunLoopBeforeSources \n");
            break;
        case kCFRunLoopBeforeWaiting:
            printf("callback---kCFRunLoopBeforeWaiting \n");
            break;
        case kCFRunLoopAfterWaiting:
            printf("callback---kCFRunLoopAfterWaiting \n");
            break;
        case kCFRunLoopExit:
            printf("callback---kCFRunLoopExit \n");
            break;
            
        default:
            break;
    }
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    
//    CFRunLoopRef runloop = CFRunLoopGetCurrent();
//    static CFRunLoopObserverRef defaultModeObserver;
//    CFRunLoopRemoveObserver(runloop, rrr, kCFRunLoopCommonModes);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
