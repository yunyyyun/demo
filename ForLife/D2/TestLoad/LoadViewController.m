//
//  LoadViewController.m
//  ForLife
//
//  Created by dc on 2021/6/3.
//

#import "LoadViewController.h"
#import "AClass.h"
#import <objc/runtime.h>

@interface LoadViewController ()

@end

@implementation LoadViewController

+ (void)load {
    NSLog(@"load0 %@", self);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
//    AClass *currentClass = [AClass class];
//    while (currentClass) {
//        unsigned int methodCount;
//        Method *methodList = class_copyMethodList(currentClass, &methodCount);
//        unsigned int i = 0;
//        for (; i < methodCount; i++) {
//            NSLog(@"%@ - %@", [NSString stringWithCString:class_getName(currentClass) encoding:NSUTF8StringEncoding],
//                  [NSString stringWithCString:sel_getName(method_getName(methodList[i])) encoding:NSUTF8StringEncoding]);
//        }
//
//        free(methodList);
//        currentClass = class_getSuperclass(currentClass);
//    }
    
    
    
    
    Class cls = [AClass class];
    
    void *yun = &cls;
    [(__bridge id)yun print0];
    
    AClass *obj = [[AClass alloc] init];
    [obj print0];
    
    NSLog(@"-----> %p-%p-%p", &obj, yun, &self);
    
    void *sp = (void *)&self;
    void *end = (void *)&obj;
    long count = (sp - end) / 0x8;
    for (long i=0; i<count; ++i) {
        void *tmp = sp - i*0x8;
        if (i==1) {
            NSLog(@"----->%ld %p:%s", i, tmp, *(char **)tmp);
        } else {
            NSLog(@"----->%ld %p:%@", i, tmp, *(void **)tmp);
        }
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
