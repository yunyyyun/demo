//
//  AClass.h
//  ForLife
//
//  Created by dc on 2021/6/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AClass : NSObject

@property (copy, nonatomic) NSString *yun_name;
// @property (assign, nonatomic) int yun_age;
- (void) fun0;

- (void) print0;

@end

NS_ASSUME_NONNULL_END
