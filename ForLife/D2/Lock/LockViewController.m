//
//  LockViewController.m
//  ForLife
//
//  Created by dc on 2022/4/16.
//

#import "LockViewController.h"

@interface LockViewController ()

@end

@implementation LockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    [self test_ss];
}

-(void) test_ts {
    int count = 3;
    NSConditionLock *lock = [[NSConditionLock alloc] initWithCondition: count];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        static void (^testMethod)(int);
        testMethod = ^(int value){
            if (value > -1) {
                [lock lockWhenCondition: value];
                NSLog(@"before = %d",value);
                NSLog(@"current taskId is = %d",value);
                value = value-1;
                NSLog(@"after = %d",value);
                [lock unlockWithCondition: value];
                testMethod(value);
            }
        };
        testMethod(count);
    });
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [lock lockWhenCondition: 0];
        NSLog(@"all task comp!");
        [lock unlockWithCondition: 0];
    });
}

-(void) test_ss {
    NSRecursiveLock *lock = [[NSRecursiveLock alloc] init];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        static void (^testMethod)(int);
        testMethod = ^(int value){
            [lock lock];
            if (value > 0) {
                NSLog(@"current value = %d",value);
                testMethod(value - 1);
            }
            [lock unlock];
        };
        testMethod(100);
    });
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
