//
//  TestViewController.m
//  ForLife
//
//  Created by meng yun on 2021/4/15.
//

#import "TestViewController.h"
#import "VideoRenderView.h"
#import <objc/runtime.h>

#define LOOP(X, n) for (int i = 0; i<n; ++i) { X; }

@interface TestViewController ()

typedef void(^Block)(int);

@property (copy , nonatomic) Block blk;

@property (assign , nonatomic) int count;

@property (assign , nonatomic) int* pa;

@property (assign , nonatomic) int a;

@property (nonatomic, copy) NSMutableString *ms;

@property (nonatomic, copy) NSString *str2Copy;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    __block int a = 0;
    NSLog(@"定义前:%p", &a); //栈区
    void (^foo)(void) = ^{
        a = 1;
        NSLog(@"block内部:%p", &a); //堆区
    };
    NSLog(@"定义后:%p", &a); //堆区
    foo();
    
    VideoRenderView *mtRenderView = [[VideoRenderView alloc] initWithFrame: CGRectMake(100, 340, 200, 200) shaderFuncName: @"fsh_normal2"];
    [self.view addSubview: mtRenderView];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        UIImage *img0 = [UIImage imageNamed: @"123"];
        [mtRenderView displayImage: img0];
    });
    
    UIButton *btn = [[UIButton alloc] initWithFrame: CGRectMake(100, 100, 200, 200)];
    [self.view addSubview: btn];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: true];
    [self test];
}

- (void) test {
    
    dispatch_queue_t
    queue = dispatch_queue_create("com.aaa.cn", DISPATCH_QUEUE_CONCURRENT);
    
    for (int i = 0; i<10000; i++) {
        dispatch_async(queue, ^{
            self.str2Copy = [NSString stringWithFormat:@"abcdefghiasd啊速度快过张满屏啊实打实的。sssabcdefghiasd啊速度快过张满屏啊实打实的。sss"];  // alloc 堆 iOS优化 - taggedpointer
            NSLog(@"%p: %@", self.str2Copy, self.str2Copy);
        });
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
