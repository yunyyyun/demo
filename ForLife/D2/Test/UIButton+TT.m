//
//  UIButton+TT.m
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import "UIButton+TT.h"
#import <objc/runtime.h>

@implementation UIButton (TT)

+ (void)load {
    // 方法替换处load不需要调用super，否则会导致父类被重复Swizzling
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{//保证方法替换只被执行一次
        [self swizzleInstanceMethodWithOriginalSEL: @selector(objectAtIndex:) SwizzleNewSEL: @selector(objectAtIndex:)];
    });
}

+ (void)swizzleInstanceMethodWithOriginalSEL:(SEL)originalSel SwizzleNewSEL:(SEL)newSel {
    
    Method originalMethod = class_getInstanceMethod(self, originalSel);
    Method newMethod = class_getInstanceMethod(self, newSel);
    if (!originalMethod || !newMethod) {
        return;
    }
    //加一层保护措施，如果添加成功，则表示该方法不存在于本类，而是存在于父类中，不能交换父类的方法,否则父类的对象调用该方法会crash；添加失败则表示本类存在该方法
    BOOL addMethod = class_addMethod(self, originalSel, method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    if (addMethod) {
        //再将原有的实现替换到swizzledMethod方法上，从而实现方法的交换，并且未影响到父类方法的实现
        class_replaceMethod(self, newSel, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    }else{
        method_exchangeImplementations(originalMethod, newMethod);
    }
    
}

@end
