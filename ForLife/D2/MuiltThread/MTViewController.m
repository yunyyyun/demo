//
//  MTViewController.m
//  ForLife
//
//  Created by dc on 2021/7/17.
//

#import "MTViewController.h"

@interface MTViewController ()

@end

@implementation MTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    // [self testPerformSel];
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        NSLog(@"--------start");
//        [self performSelector:@selector(_sel1) ];
//        [self performSelector:@selector(_sel2) withObject: nil afterDelay:0];
//        // [self performSelectorOnMainThread: @selector(_sel3) withObject: nil waitUntilDone: false];
//        sleep(2);
//        NSLog(@"--------end");
//    });
    
    dispatch_semaphore_t sp = dispatch_semaphore_create(1);
    dispatch_semaphore_signal(sp);
    dispatch_semaphore_signal(sp);
    
    NSString *notificationName = @"AAAA";
    // [[NSNotificationCenter defaultCenter] postNotificationName: notificationName object: nil userInfo: @{@"key": @"11"}];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = 0; i < 2; ++i) {
            NSLog(@"postNoti---: %d %@", i, [NSThread currentThread]);
            [[NSNotificationCenter defaultCenter] postNotificationName: notificationName object: nil userInfo: @{@"key": @"123456"}];
        }
    });
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = 110; i < 112; ++i) {
            NSLog(@"postNoti---: %d %@", i, [NSThread currentThread]);
            [[NSNotificationCenter defaultCenter] postNotificationName: notificationName object: nil userInfo: @{@"key": @"123456"}];
        }
    });
    [[NSNotificationCenter defaultCenter] postNotificationName: nil object: nil userInfo: @{@"key": @"dddd"}];
    // [[NSNotificationCenter defaultCenter] postNotificationName: notificationName object: nil userInfo: @{@"key": @"333"}];
    // [self test_group];
}

// dispatch_group_t
- (void) test_group {
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    
    dispatch_group_enter(group);
    dispatch_async(queue, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"-dispatch_async-: %d %@", i, [NSThread currentThread]);
        }
        dispatch_group_leave(group);
    });
    
    dispatch_group_notify(group, queue, ^{
        //执行D
        NSLog(@"-dispatch_group_notify-: %d %@", 0, [NSThread currentThread]);
        dispatch_async(queue, ^{
            for (int i = 0; i < 8; ++i) {
                NSLog(@"-dispatch_group_notify----------");
            }
        });
    });
   
}

-(void) test_ss {
    
    NSLock *lock = [[NSLock alloc] init];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        static void (^testMethod)(int);
        testMethod = ^(int value){
            [lock lock];
            if (value > 0) {
                NSLog(@"current value = %d",value);
                testMethod(value - 1);
            }
            [lock unlock];
        };
        testMethod(10);
    });
}

-(void) test_semaphore {
    dispatch_queue_t q = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    dispatch_semaphore_t sp = dispatch_semaphore_create(1);
    dispatch_async(q, ^{
        dispatch_semaphore_wait(sp, DISPATCH_TIME_FOREVER);
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
        dispatch_semaphore_signal(sp);
    });
    dispatch_async(q, ^{
        dispatch_semaphore_wait(sp, DISPATCH_TIME_FOREVER);
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------2-: %d %@", i, [NSThread currentThread]);
        }
        dispatch_semaphore_signal(sp);
    });
}

-(void) test_lock {
    dispatch_queue_t q = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    NSLock *lock = [[NSLock alloc] init];
    dispatch_async(q, ^{
        [lock lock];
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
        [lock unlock];
    });
    dispatch_async(q, ^{
        [lock lock];
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------2-: %d %@", i, [NSThread currentThread]);
        }
        [lock unlock];
    });
    
    dispatch_barrier_async(q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------3-: %d %@", i, [NSThread currentThread]);
        }
    });
    
    dispatch_async(q, ^{
        [lock lock];
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------4-: %d %@", i, [NSThread currentThread]);
        }
        [lock unlock];
    });
}

-(void) test_barrier {
    dispatch_queue_t q = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
    });
    dispatch_async(q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------2-: %d %@", i, [NSThread currentThread]);
        }
    });
    
    dispatch_barrier_async(q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------3-: %d %@", i, [NSThread currentThread]);
        }
    });
    
    dispatch_async(q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------4-: %d %@", i, [NSThread currentThread]);
        }
    });
}

-(void) tongbu_chuanxingduilie {
    dispatch_queue_t qq = dispatch_queue_create("test", DISPATCH_QUEUE_SERIAL);
    dispatch_async(qq, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
    });
    dispatch_async(qq, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------2-: %d %@", i, [NSThread currentThread]);
        }
    });
    dispatch_async(qq, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------3-: %d %@", i, [NSThread currentThread]);
        }
    });
}

// 同步：dispatch_group_t
-(void) tongbu_gcd {
    dispatch_group_t g = dispatch_group_create();
    dispatch_queue_t q = dispatch_get_global_queue(0, 0);
    
    dispatch_queue_t qq = dispatch_queue_create("test", DISPATCH_QUEUE_CONCURRENT);
    dispatch_group_async(g, q, ^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
//        dispatch_async(qq, ^{
//            for (int i = 0; i < 8; ++i) {
//                NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
//            }
//        });
    });
    dispatch_group_async(g, q, ^{
        dispatch_async(qq, ^{
            for (int i = 0; i < 8; ++i) {
                NSLog(@"---------122-: %d %@", i, [NSThread currentThread]);
            }
        });
    });
    NSLog(@"111%@", [NSThread currentThread]);
    dispatch_group_notify(g, q, ^{
        // dispatch_async(qq, ^{
            NSLog(@"222%@", [NSThread currentThread]);
        // });
    });
}

// 同步：dispatch_group_t
-(void) tongbu_NSOperationQueue {
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    NSBlockOperation *op1 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------1-: %d %@", i, [NSThread currentThread]);
        }
    }];
    
    NSBlockOperation *op2 = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i < 8; ++i) {
            NSLog(@"---------2-: %d %@", i, [NSThread currentThread]);
        }
    }];
    
    [op1 addDependency: op2];
    
    op2.completionBlock = ^{
        NSLog(@"全部完成，线程：%@", [NSThread currentThread]);
    };
    
    // 添加操作到队列
    [q addOperation: op1];
    [q addOperation: op2];

}

-(void) _sel1 {
    sleep(6);
    NSLog(@"call _sel1 %@", [NSThread currentThread]);
}
-(void) _sel2 {
    NSLog(@"call _sel2 %@", [NSThread currentThread]);
}
-(void) _sel3 {
    sleep(6);
    NSLog(@"call _sel3 %@", [NSThread currentThread]);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
