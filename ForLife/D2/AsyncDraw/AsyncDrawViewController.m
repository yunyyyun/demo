//
//  AsyncDrawViewController.m
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import "AsyncDrawViewController.h"
#import "AwesomeView.h"
#import "AsyncDrawView.h"

@interface AsyncDrawViewController ()

@property (nonatomic, strong) AwesomeView *aView;

@end

@implementation AsyncDrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    _aView = [[AwesomeView alloc] initWithFrame: self.view.bounds];
    [self.view addSubview: _aView];
    [_aView.layer setNeedsDisplay];
    
//    AsyncDrawView *bView = [[AsyncDrawView alloc] initWithFrame: self.view.bounds];
//    [self.view addSubview: bView];
//    [bView.layer setNeedsDisplay];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
