//
//  AwesomeDrawData.h
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AwesomeView.h"
#import "AwesomePoint.h"
#import "AwesomeLine.h"
#import "AwesomeCircle.h"

NS_ASSUME_NONNULL_BEGIN

@interface AwesomeDrawData : NSObject

@property(nonatomic, assign) CGFloat pointSize;
@property(nonatomic, strong) NSMutableSet<AwesomePoint *> *points;
@property(nonatomic, strong) NSMutableDictionary *lines;
@property(nonatomic, strong) NSMutableSet<AwesomeCircle *> *circles;
@property(nonatomic, strong) NSMutableSet<AwesomeCircle *> *tmpCircles;

@end

NS_ASSUME_NONNULL_END
