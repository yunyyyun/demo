//
//  AwesomeCircle.h
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AwesomeCircle : NSObject

@property (nonatomic, assign)CGPoint center;
@property (nonatomic, assign)CGFloat radius;

@property (nonatomic, assign)BOOL needDraw;

- (AwesomeCircle *)initWithCenter: (CGPoint)center radius: (CGFloat)radius;

@end

NS_ASSUME_NONNULL_END
