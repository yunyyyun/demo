//
//  AwesomePoint.m
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import "AwesomePoint.h"

@implementation AwesomePoint

- (AwesomePoint *)initWithPoint: (CGPoint)point{
    AwesomePoint *p = [[AwesomePoint alloc] init];
    p.x = point.x;
    p.y = point.y;
    p.p = point;
    p.needDraw = true;
    return p;
}

@end
