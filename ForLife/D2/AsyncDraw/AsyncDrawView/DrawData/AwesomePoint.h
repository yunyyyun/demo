//
//  AwesomePoint.h
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AwesomePoint : NSObject

@property (nonatomic, assign)CGFloat x;
@property (nonatomic, assign)CGFloat y;
@property (nonatomic, assign)CGPoint p;
@property (nonatomic, assign)BOOL needDraw;

- (AwesomePoint *)initWithPoint: (CGPoint)point;

@end

NS_ASSUME_NONNULL_END
