//
//  AwesomeLine.h
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AwesomeLine : NSObject

@property (nonatomic, assign)CGPoint start;
@property (nonatomic, assign)CGPoint end;
@property (nonatomic, assign)BOOL needDraw;

- (AwesomeLine *)initWithStart: (CGPoint)start end: (CGPoint)end;

@end

NS_ASSUME_NONNULL_END
