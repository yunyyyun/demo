//
//  AsyncDrawView.m
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import "AsyncDrawView.h"
#import "AsyncDrawDelegate.h"

@interface AsyncDrawView()

@property (nonatomic, strong) AsyncDrawDelegate *drawDelegate;

@end

@implementation AsyncDrawView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if (self) {
        self.backgroundColor = [[UIColor systemGreenColor] colorWithAlphaComponent: 0.1];
        self.drawDelegate = [[AsyncDrawDelegate alloc] init];
        self.layer.delegate = self.drawDelegate;
    }
    return self;
}

@end
