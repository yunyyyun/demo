//
//  AsyncDrawDelegate.h
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AwesomeDrawData.h"

NS_ASSUME_NONNULL_BEGIN

@interface AsyncDrawDelegate : NSObject <CALayerDelegate>

@property (nonatomic, strong) AwesomeDrawData *data;
- (void) prepareDatas;

@end

NS_ASSUME_NONNULL_END
