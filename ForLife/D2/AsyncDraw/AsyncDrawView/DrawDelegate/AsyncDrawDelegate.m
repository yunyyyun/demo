//
//  AsyncDrawDelegate.m
//  ForLife
//
//  Created by dc on 2022/5/3.
//

#import "AsyncDrawDelegate.h"
#import <CoreText/CoreText.h>
#import "AwesomePoint.h"
#import "AwesomeLine.h"
#import "AwesomeCircle.h"

@implementation AsyncDrawDelegate

- (void) prepareDatas {
    _data = [[AwesomeDrawData alloc] init];
    
    AwesomeCircle *c0 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(100, 100) radius: 222];
    AwesomeCircle *c1 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(150, 200) radius: 111];
    AwesomeCircle *c2 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(100, 300) radius: 133];
    AwesomeCircle *c3 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(150, 400) radius: 222];
    AwesomeCircle *c4 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(100, 500) radius: 99];
    AwesomeCircle *c5 = [[AwesomeCircle alloc] initWithCenter: CGPointMake(333, 777) radius: 222];
    _data.circles = [[NSMutableSet alloc] initWithArray: @[c0, c1, c2, c3, c4, c5]];
    
    // _data.lines = @[];
}

- (void)displayLayer:(CALayer *)layer {
    [self prepareDatas];
    
    CGSize size = layer.bounds.size;
    CGFloat scale = [UIScreen mainScreen].scale;
    // 异步绘制，切换至子线程
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        UIGraphicsBeginImageContextWithOptions(size, NO, scale);
        // 获取当前上下文
        // CGContextRef context = UIGraphicsGetCurrentContext();
        [self drawCircles];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        // 子线程完成工作，切换至主线程显示
        dispatch_async(dispatch_get_main_queue(), ^{
            layer.contents = (__bridge id)image.CGImage;
        });
    });
}

- (void)drawCircles {
    UIColor *_pointColor = [[UIColor systemBlueColor] colorWithAlphaComponent: 0.6];
    UIColor * _fillColor = [[UIColor blackColor] colorWithAlphaComponent: 0.3];
    CGFloat _pointSize = 2.0f;
    CGFloat _lineWidth = 1.0f;
    CGFloat _blendDepth = 0.5f;

    // 画圆
    for (AwesomeCircle *c in _data.circles){
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        bezierPath.lineWidth = _lineWidth;
        [bezierPath addArcWithCenter: c.center radius: _pointSize*0.5 startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [_pointColor setFill];
        [bezierPath fill];
        
        [bezierPath removeAllPoints];
        CGFloat rr = c.radius;
        [bezierPath addArcWithCenter: c.center radius: rr startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [bezierPath strokeWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
        [_fillColor setFill];
        [bezierPath fillWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
    }
}

- (void)drawLines {
    UIColor *_pointColor = [[UIColor systemBlueColor] colorWithAlphaComponent: 0.6];
    UIColor * _fillColor = [[UIColor blackColor] colorWithAlphaComponent: 0.3];
    CGFloat _pointSize = 2.0f;
    CGFloat _lineWidth = 1.0f;
    CGFloat _blendDepth = 0.5f;
    
    for(NSString *key in _data.lines)
    {
        // NSLog(@"bezierDraw_key:%@ value:%@",key,_lines[key]);
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        bezierPath.lineWidth = _lineWidth;
        NSArray *curLines = _data.lines[key];
        for (int i=0; i<curLines.count; ++i){
            AwesomeLine *l = curLines[i];
            CGPoint start = l.start;
            CGPoint end = l.end;
            if (i==0){
                [bezierPath moveToPoint: start];
                [bezierPath addLineToPoint: end];
            }
            else{
                [bezierPath addLineToPoint: start];
                if (i==curLines.count-1){
                    [bezierPath addLineToPoint: end];
                    // [bezierPath closePath];
                    l.needDraw = false;
                }
            }
        }
        [bezierPath strokeWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
    }
}

- (void)draw:(CGContextRef)context size:(CGSize)size {
    // 将坐标系上下翻转，因为底层坐标系和 UIKit 坐标系原点位置不同。
    CGContextSetTextMatrix(context, CGAffineTransformIdentity);
    // 文本沿着Y轴移动
    CGContextTranslateCTM(context, 0, size.height); // 原点为左下角
    // 文本反转成context坐标系
    CGContextScaleCTM(context, 1, -1);
    // 创建绘制区域
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0, 0, size.width, size.height));
    // 创建需要绘制的文字
    NSString *text = @"AsyncDraw Test \nAsyncDraw Test \nAsyncDraw Test \nAsyncDraw Test \nAsyncDraw Test";
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]initWithString: text];
    [attrStr addAttribute:NSFontAttributeName value: [UIFont systemFontOfSize: 33] range:NSMakeRange(0, text.length)];
    // 根据attStr生成CTFramesetterRef
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)attrStr);
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, attrStr.length), path, NULL);
    // 将frame的内容绘制到content中
    CTFrameDraw(frame, context);
}

@end
