//
//  AwesomeView.h
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AwesomeView : UIView

- (void) clean;
- (void) adjust;
- (void) setShowPoint;

@end

NS_ASSUME_NONNULL_END
