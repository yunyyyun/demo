//
//  WeakTimerTarget.m
//  ForLife
//
//  Created by dc on 2021/6/4.
//

#import "WeakTimerTarget.h"

@implementation WeakTimerTarget

- (void) fire:(NSTimer *)timer {
    if(self.target) {
        [self.target performSelector: self.selector withObject:timer.userInfo];
    } else {
        [self.timer invalidate];
    }
}

-(void)dealloc {
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}


+ (NSTimer *) scheduledTimerWithTimeInterval: (NSTimeInterval)interval
                                      target: (id)aTarget
                                    selector: (SEL)aSelector
                                    userInfo: (id)userInfo
                                     repeats: (BOOL)repeats {
    WeakTimerTarget* timerTarget = [WeakTimerTarget alloc];
    timerTarget.target = aTarget;
    timerTarget.selector = aSelector;
    timerTarget.timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                         target:timerTarget
                                                       selector:@selector(fire:)
                                                       userInfo:userInfo
                                                        repeats:repeats];
    return timerTarget.timer;
}

+ (NSTimer *)scheduledTimerWithTimeInterval: (NSTimeInterval)interval
                                      block: (HWTimerHandler)block
                                   userInfo: (id)userInfo
                                    repeats: (BOOL)repeats {
    return [self scheduledTimerWithTimeInterval: interval
                                         target: self
                                       selector: @selector(_timerBlockInvoke:)
                                       userInfo: @[[block copy], userInfo]
                                        repeats: repeats];
}

+ (void)_timerBlockInvoke:(NSArray*)userInfo {
    HWTimerHandler block = userInfo[0];
    id info = userInfo[1];
    // or `!block ?: block();` @sunnyxx
    if (block) {
        block(info);
    }
}

@end
