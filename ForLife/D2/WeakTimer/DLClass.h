//
//  DLClass.h
//  ForLife
//
//  Created by dc on 2021/6/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DLClass : NSObject

+ (id) shared;

@end

NS_ASSUME_NONNULL_END
