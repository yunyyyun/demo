//
//  WeakTimerViewController.m
//  ForLife
//
//  Created by dc on 2021/6/3.
//

#import "WeakTimerViewController.h"
#import "WeakTimerTarget.h"

@interface WeakTimerViewController ()

@property (nonatomic, weak) NSTimer* timer;

@end

@implementation WeakTimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
//    __weak typeof(self) weakSelf = self;  // weak 并不能解决循环引用
//    _timer = [NSTimer scheduledTimerWithTimeInterval: 3.0f
//                                              target: weakSelf
//                                            selector: @selector(timerFire:)
//                                            userInfo: nil
//                                             repeats: YES];
//    [_timer fire];
    
    
    _timer = [WeakTimerTarget scheduledTimerWithTimeInterval: 1
                                                      target: self
                                                    selector: @selector(timerFire:)
                                                    userInfo: nil
                                                     repeats: true];
     [_timer fire];
}

-(void)timerFire:(id)userinfo {
    NSLog(@"tick---Fire");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
}

-(void)dealloc {
    [_timer invalidate];
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
