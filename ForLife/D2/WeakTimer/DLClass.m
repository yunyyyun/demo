//
//  DLClass.m
//  ForLife
//
//  Created by dc on 2021/6/8.
//

#import "DLClass.h"

@implementation DLClass

+ (id)shared {
    static dispatch_once_t dt;
    static DLClass *_instance;
    dispatch_once(&dt, ^{
        _instance = [[DLClass alloc] init];
    });
    
    return _instance;
}

@end
